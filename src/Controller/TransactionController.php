<?php

namespace Bitkorn\PayPalCheckout\Controller;

use Bitkorn\PayPalCheckout\Service\PayPalClientService;
use Bitkorn\Shop\Controller\AbstractShopController;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Sample\CaptureIntentExamples\CreateOrder;

class TransactionController extends AbstractShopController
{
    /**
     * @var bool
     */
    protected $debug;

    /**
     * @var PayPalClientService
     */
    protected $payPalClientService;

    /**
     * @var ShopService
     */
    protected $shopService;

    /**
     * @param bool $debug
     */
    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }

    /**
     * @param ShopService $shopService
     */
    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    /**
     * @param PayPalClientService $payPalClientService
     */
    public function setPayPalClientService(PayPalClientService $payPalClientService): void
    {
        $this->payPalClientService = $payPalClientService;
    }

    /**
     * AJAX action to create a PayPal order.
     * It persists the payment ID.
     * @return JsonModel
     */
    public function createOrderAction()
    {
        $jsonModel = new JsonModel();
        if (empty($xshopBasketEntity = $this->basketService->checkBasket()) || empty($xshopBasketEntity->getStorageItems())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setVariable('orderID', $this->payPalClientService->createOrder($xshopBasketEntity));
        return $jsonModel;
    }

    /**
     * Capture Transaction on the same server (URL) like the orderRequest. Therefore we can use session-basket_unique.
     *
     *  {
     *      "orderID": "4RA93998MR6534335",
     *      "payerID": "LWUVXVWJTKER8",
     *      "paymentID": null,
     *      "billingToken": null,
     *      "facilitatorAccessToken": "A21AAErAWnzp1AyDEPj_aKk4KKEwmYHrNTw_r7nfJt9BxerWewMwlDlPkjWwLzXduoCHE5cnjCn7Oazob9ANDukrO9pkFfm5w"
     *  }
     * @return JsonModel
     */
    public function captureTransactionAction()
    {
        $jsonModel = new JsonModel();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $orderId = $this->params('order_id');
            $response = $this->payPalClientService->captureOrder($orderId);
        }

        return $jsonModel;
    }

    public function requestReturnAction()
    {
        $jsonModel = new JsonModel();
        $this->logger->info(__FUNCTION__ . '() ' . print_r($_REQUEST, true));
        return $jsonModel;
    }

    public function requestCancelAction()
    {
        $jsonModel = new JsonModel();
        $this->logger->info(__FUNCTION__ . '() ' . print_r($_REQUEST, true));
        return $jsonModel;
    }
}
