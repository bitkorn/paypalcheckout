<?php

namespace Bitkorn\PayPalCheckout\Service;

use Bitkorn\Shop\Entity\Address\ShopAddressEntity;
use Bitkorn\Shop\Entity\Basket\XshopBasketEntity;
use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Shipping\ShippingProvider\AbstractShippingProvider;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Tools\Http\HttpTool;
use Laminas\View\Helper\BasePath;
use Laminas\View\Helper\Url;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalHttp\HttpResponse;

class PayPalClientService extends AbstractService
{
    protected string $clientId;
    protected string $clientSecret;
    protected bool $debug = true;
    protected XshopBasketEntity $basketEntity;
    protected Url $url;
    protected ShopService $shopService;
    protected BasketService $basketService;
    protected BasketFinalizeService $basketFinalizeService;
    protected ShopAddressService $shopAddressService;
    protected ShippingService $shippingService;

    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function setClientSecret(string $clientSecret): void
    {
        $this->clientSecret = $clientSecret;
    }

    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }

    public function setUrl(Url $url): void
    {
        $this->url = $url;
    }

    public function setShopService(ShopService $shopService): void
    {
        $this->shopService = $shopService;
    }

    public function setBasketService(BasketService $basketService): void
    {
        $this->basketService = $basketService;
    }

    public function setBasketFinalizeService(BasketFinalizeService $basketFinalizeService): void
    {
        $this->basketFinalizeService = $basketFinalizeService;
    }

    public function setShippingService(ShippingService $shippingService): void
    {
        $this->shippingService = $shippingService;
    }

    /**
     * @param ShopAddressService $shopAddressService
     */
    public function setShopAddressService(ShopAddressService $shopAddressService): void
    {
        $this->shopAddressService = $shopAddressService;
    }

    /**
     * Returns PayPal HTTP client instance with environment which has access
     * credentials context. This can be used invoke PayPal API's provided the
     * credentials have the access to do so.
     */
    protected function client(): PayPalHttpClient
    {
        return new PayPalHttpClient($this->environment());
    }

    /**
     * Setting up and Returns PayPal SDK environment with PayPal Access credentials.
     * For demo purpose, we are using SandboxEnvironment. In production this will be
     * ProductionEnvironment.
     */
    protected function environment(): SandboxEnvironment
    {
        return new SandboxEnvironment($this->clientId, $this->clientSecret);
    }

    protected function buildRequestBody(): array
    {
        if (!isset($this->basketEntity) || !$this->basketEntity instanceof XshopBasketEntity) {
            throw new \Exception(__CLASS__ . '()->' . __FUNCTION__ . '() called without having set the BasketEntity.');
        }
        $items = $this->basketEntity->getStorageItems();
        $address = $this->shopAddressService->getShopAddressEntity($this->basketEntity);
        $shippingProvider = $this->shippingService->getShippingProviderInstance($this->basketEntity->getShippingProviderUniqueDescriptor());
        if (
            empty($items) || !isset($items[0]['computed_value_article_price_total_sum_end'])
            || empty($address) || !$address instanceof ShopAddressEntity
            || empty($shippingProvider) || !$shippingProvider instanceof AbstractShippingProvider
        ) {
            throw new \Exception(__CLASS__ . '()->' . __FUNCTION__ . '() called neither with empty basket nor without address nor shipping provider.');
        }
        $baseUrl = HttpTool::getBaseUrl();
        $rb = [];
        $rb['intent'] = 'CAPTURE';
        $rb['application_context'] = [
            'brand_name' => $this->shopService->getShopConfigurationValue('shop_owner'),
            'locale' => 'de-DE',
            'landing_page' => 'BILLING',
            'shipping_preferences' => $address->getAddressLine(),
            'user_action' => 'PAY_NOW',
            'return_url' => $baseUrl . call_user_func($this->url, 'bitkorn_paypalcheckout_transaction_requestreturn'),
            'cancel_url' => $baseUrl . call_user_func($this->url, 'bitkorn_paypalcheckout_transaction_requestcancel')
        ];
        /**
         * https://developer.paypal.com/docs/api/orders/v2/#definition-purchase_unit
         */
        $rb['purchase_units'] = [];
        $pu = [
            'reference_id' => $this->basketEntity->getBasketUnique(),
            'description' => $this->shopService->getShopConfigurationValue('shop_name'),
//            'custom_id' => 'CUST-HighFashions',
//            'soft_descriptor' => 'HighFashions',
            'amount' =>
                [
                    'currency_code' => 'EUR',
                    'value' => $this->basketEntity->getTotalCosts(),
//                    'breakdown' =>
//                        [
//                            'item_total' =>
//                                [
//                                    'currency_code' => 'EUR',
//                                    'value' => round($this->basketEntity->getArticlePriceTotalSumEnd()),
//                                ],
//                            'shipping' =>
//                                [
//                                    'currency_code' => 'EUR',
//                                    'value' => round($this->basketEntity->getArticleShippingCostsComputed()),
//                                ],
//                            'handling' =>
//                                [
//                                    'currency_code' => 'EUR',
//                                    'value' => '0',
//                                ],
//                            'tax_total' =>
//                                [
//                                    'currency_code' => 'EUR',
//                                    'value' => round($this->basketEntity->getBasketTaxTotalSumEnd()),
//                                ],
//                        ],
                ],
        ];

        $items = [];
        foreach ($this->basketEntity->getStorageItems() as $item) {
            $items[] = [
                'name' => $item['shop_article_name'],
                'sku' => $item['shop_article_sku'],
                'unit_amount' =>
                    [
                        'currency_code' => 'EUR',
                        'value' => round($item['computed_value_article_price_total'], 2),
                    ],
                'tax' =>
                    [
                        'currency_code' => 'EUR',
                        'value' => round($item['computed_value_article_tax_total'], 2),
                    ],
                'quantity' => $item['computed_value_article_amount'], // if decimal THEN Request is not well-formed, syntactically incorrect, or violates schema
            ];
        } // end foreach

        /**
         * If item details are specified (items.unit_amount and items.quantity) corresponding amount.breakdown.item_total is required
         */
//        $pu['items'] = $items;
        $pu['shipping'] = [
            'method' => $shippingProvider->getBrandText(),
            'address' =>
                [
                    'address_line_1' => $address->getAddressStreetWithNumber(),
                    'address_line_2' => $address->getAddressName2(),
                    'admin_area_2' => $address->getAddressCity(),
//                    'admin_area_1' => $address->getCountryName(), // The highest level sub-division in a country, which is usually a province, state, or ISO-3166-2 subdivision. Format for postal delivery. E.g. NRW
                    'postal_code' => $address->getAddressZip(),
                    'country_code' => $address->getCountryIso(),
                ],
        ];
        $rb['purchase_units'][] = $pu;
        return $rb;
    }

    public function createOrder(XshopBasketEntity $basketEntity): string
    {
        $this->basketEntity = $basketEntity;
        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = $this->buildRequestBody();
        if ($this->debug) {
            $this->logger->info('CREATE Order Request->body: ' . print_r($request->body, true));
        }
        $client = $this->client();
        $response = $client->execute($request);
        if ($this->debug) {
            $this->logger->debug('CREATE Order Response->result: ' . json_encode($response->result, JSON_PRETTY_PRINT));
        }

        if (
            empty($response->result) || empty($response->result->id)
            || !isset($response->result->purchase_units[0]) // there is always only one purchase_unit (see $this->buildRequestBody())
            || empty($basketHash = $response->result->purchase_units[0]->reference_id)
            || $basketHash != $basketEntity->getBasketUnique()
        ) {
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Some of the response is incorrect. Basket ' . $basketEntity->getBasketUnique());
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() Some of the response is incorrect. Basket ' . $basketEntity->getBasketUnique());
        }
        $orderID = $response->result->id;
        $this->basketService->setPaymentMethod('PayPal');
        $this->basketService->setPaymentNumber($orderID);
        $this->basketFinalizeService->basketOrdered($this->basketEntity->getBasketUnique());
        return $orderID;
    }

    public function captureOrder(string $orderId): bool
    {
        $request = new OrdersCaptureRequest($orderId);

        $client = $this->client();
        $response = $client->execute($request);
        if ($this->debug) {
            $this->logger->debug('CAPTURE Order Response->result: ' . json_encode($response->result, JSON_PRETTY_PRINT));
        }
//        if(strtoupper($response->result->status) == 'COMPLETED') {
//            $this->logger->info('COMPLETED');
//        }
//        if(isset($response->result->purchase_units[0])) {
//            $this->logger->info('purchase_units[0]');
//        }
//        if(!empty($response->result->purchase_units[0]['reference_id'])) {
//            $this->logger->info('purchase_units[0][\'reference_id\']');
//        }
        if (
            strtoupper($response->result->status) == 'COMPLETED'
            && isset($response->result->purchase_units[0])
            && !empty($response->result->purchase_units[0]->reference_id)
        ) {
            $this->basketFinalizeService->basketPayed(
                $response->result->purchase_units[0]->reference_id, time(),
                'payerID: ' . $response->result->payer->payer_id
            );
        } else {
            return false;
        }

        return true;
    }
}
