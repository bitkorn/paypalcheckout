<?php

namespace Bitkorn\PayPalCheckout\View\Helper;

use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;

class PayPalBrand extends AbstractViewHelper
{

    const TEMPLATE = 'template/paypal-brand';

    /**
     * @var string
     */
    protected $clientId;

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }

    public function __invoke()
    {
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);
        $viewModel->setVariable('clientId', $this->clientId);

        return $this->getView()->render($viewModel);
    }
}
