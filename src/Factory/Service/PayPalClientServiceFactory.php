<?php

namespace Bitkorn\PayPalCheckout\Factory\Service;

use Bitkorn\PayPalCheckout\Service\PayPalClientService;
use Bitkorn\Shop\Service\Address\ShopAddressService;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\Shipping\ShippingService\ShippingService;
use Bitkorn\Shop\Service\ShopService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;

class PayPalClientServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new PayPalClientService();
		$service->setLogger($container->get('logger'));
		$ppConfig = $container->get('config')['bitkorn_paypalcheckout'];
        $service->setClientId($ppConfig['client_id']);
        $service->setClientSecret($ppConfig['client_secret']);
        $service->setDebug($ppConfig['debug']);
        /** @var HelperPluginManager $plugins */
        $plugins = $container->get('ViewHelperManager');
        $service->setUrl($plugins->get('url'));
        $service->setShopService($container->get(ShopService::class));
        $service->setBasketService($container->get(BasketService::class));
        $service->setBasketFinalizeService($container->get(BasketFinalizeService::class));
        $service->setShippingService($container->get(ShippingService::class));
        $service->setShopAddressService($container->get(ShopAddressService::class));
		return $service;
	}
}
