<?php

namespace Bitkorn\PayPalCheckout\Factory\View\Helper;

use Bitkorn\PayPalCheckout\View\Helper\PayPalBrand;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PayPalBrandFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new PayPalBrand();
		$viewHelper->setLogger($container->get('logger'));
        $viewHelper->setClientId($container->get('config')['bitkorn_paypalcheckout']['client_id']);
		return $viewHelper;
	}
}
