<?php

namespace Bitkorn\PayPalCheckout\Factory\Controller;

use Bitkorn\PayPalCheckout\Controller\TransactionController;
use Bitkorn\PayPalCheckout\Service\PayPalClientService;
use Bitkorn\Shop\Service\Basket\BasketFinalizeService;
use Bitkorn\Shop\Service\Basket\BasketService;
use Bitkorn\Shop\Service\ShopService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TransactionControllerFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$controller = new TransactionController();
		$controller->setLogger($container->get('logger'));
		$controller->setUserService($container->get(UserService::class));
        $controller->setDebug($container->get('config')['bitkorn_paypalcheckout']['debug']);
        $controller->setShopService($container->get(ShopService::class));
        $controller->setBasketService($container->get(BasketService::class));
        $controller->setPayPalClientService($container->get(PayPalClientService::class));
		return $controller;
	}
}
