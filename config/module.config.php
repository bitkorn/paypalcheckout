<?php

namespace Bitkorn\PayPalCheckout;

use Bitkorn\PayPalCheckout\Controller\TransactionController;
use Bitkorn\PayPalCheckout\Factory\Controller\TransactionControllerFactory;
use Bitkorn\PayPalCheckout\Factory\Service\PayPalClientServiceFactory;
use Bitkorn\PayPalCheckout\Factory\View\Helper\PayPalBrandFactory;
use Bitkorn\PayPalCheckout\Service\PayPalClientService;
use Bitkorn\PayPalCheckout\View\Helper\PayPalBrand;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'bitkorn_paypalcheckout_transaction_createorder' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/paypalcheckout-createorder',
                    'defaults' => [
                        'controller' => TransactionController::class,
                        'action' => 'createOrder',
                    ],
                ],
            ],
            'bitkorn_paypalcheckout_transaction_capturetransaction' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/paypalcheckout-capturetransaction[/:order_id]',
                    'constraints' => [
                        'order_id' => '[0-9a-zA-Z_-]*'
                    ],
                    'defaults' => [
                        'controller' => TransactionController::class,
                        'action' => 'captureTransaction',
                    ],
                ],
            ],
            'bitkorn_paypalcheckout_transaction_requestreturn' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/paypalcheckout-requestreturn',
                    'defaults' => [
                        'controller' => TransactionController::class,
                        'action' => 'requestReturn',
                    ],
                ],
            ],
            'bitkorn_paypalcheckout_transaction_requestcancel' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/paypalcheckout-requestcancel',
                    'defaults' => [
                        'controller' => TransactionController::class,
                        'action' => 'requestCancel',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            TransactionController::class => TransactionControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            PayPalClientService::class => PayPalClientServiceFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers' => [
        'factories' => [
            PayPalBrand::class => PayPalBrandFactory::class,
        ],
        'invokables' => [],
        'aliases' => [
            'paypalSmartButtons' => PayPalBrand::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'template/paypal-brand' => __DIR__ . '/../view/template/paypal-brand.phtml',
        ],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'bitkorn_paypalcheckout' => [
        'client_id' => 'AUFx8sxwtkScNGi0eYGp36Z7Wvj_qWPsMB2hkV_wDmJ_0DbXGnCfm_Xh4H_EXgrSjSPmuxREJH7ZkvN2',
        'client_secret' => 'EFzjdSzVK6hr8viTybzKQXE5Fw74Uw9kaBhAwpfyfzXBP2Zt94AeWEmViuyYU96ZxtY0PObM8xltjsaG',
        'debug' => true,
//        'mode' => 'sandbox', // sandbox | live
    ]
];
