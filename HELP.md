
[github.com/paypal/PayPal-PHP-SDK](https://github.com/paypal/PayPal-PHP-SDK)

- [developer.paypal.com/docs](https://developer.paypal.com/docs)
  - [/api](https://developer.paypal.com/docs/api)
    - [/payments/v2](https://developer.paypal.com/docs/api/payments/v2/)
    - [/orders/v2/#orders](https://developer.paypal.com/docs/api/orders/v2/#orders)
    - [/orders/v2/#orders_create](https://developer.paypal.com/docs/api/orders/v2/#orders_create)
    - [/orders/v2/#orders_capture](https://developer.paypal.com/docs/api/orders/v2/#orders_capture)
    - [/orders/v2/#definition-purchase_unit_request](https://developer.paypal.com/docs/api/orders/v2/#definition-purchase_unit_request)
  - [/checkout/integrate](https://developer.paypal.com/docs/checkout/integrate/)
  - [/checkout/reference](https://developer.paypal.com/docs/checkout/reference/)
    - [/customize-sdk/#query-parameters](https://developer.paypal.com/docs/checkout/reference/customize-sdk/#query-parameters)
- [developer.paypal.com/docs/platforms](https://developer.paypal.com/docs/platforms)
  - [checkout/set-up-payments](https://developer.paypal.com/docs/platforms/checkout/set-up-payments/)
- [sandbox.paypal.com](https://www.sandbox.paypal.com/) login to seller/buyer account settings


The `/data/app/public/js/paypal-smart-payment-buttons.sandbox.js` is created with sandbox client ID.

For pruction use, you must create a new JS file: [Add the PayPal JavaScript SDK to your web page](https://developer.paypal.com/docs/checkout/integrate/#2-add-the-paypal-javascript-sdk-to-your-web-page)
